package com.canvalabs.dharamkanta;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SearchReceiptActivity extends AppCompatActivity {

    private EditText receiptNumberEditText;
    SharedPreferences prefs;
    private String TAG = "searchActivity";
    IntentIntegrator qrScan;
    String receiptId;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        receiptNumberEditText = findViewById(R.id.reciept_number);
        Button buttonScan = findViewById(R.id.btn_scan);

        qrScan = new IntentIntegrator(this);
        qrScan.setOrientationLocked(false);
        qrScan.setPrompt("Place your receipt here!");
        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgress.show();
                qrScan.initiateScan();
            }
        });

        Button searchBtn = findViewById(R.id.btn_search);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgress.show();
                searchReceipt("search");
            }
        });

        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Refreshing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            mProgress.dismiss();
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Data Not Found", Toast.LENGTH_LONG).show();
            } else {

                try {

//                    Toast.makeText(this, result.getContents(), Toast.LENGTH_SHORT).show();
                    receiptId = result.getContents().trim();
                    searchReceipt("scan");
                } catch (Exception e) {
                    e.printStackTrace();

                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            mProgress.dismiss();
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void searchReceipt(String val) {
        String receiptID = "";
        if (val.equals("search")){
            if (receiptNumberEditText.getText().toString().equals("")) {

                Toast.makeText(this, "Enter receipt number!", Toast.LENGTH_SHORT).show();
                return;
            }else {
                receiptID = receiptNumberEditText.getText().toString();
            }
        }else if(val.equals("scan")){
            if (receiptId.equals("")){
                Toast.makeText(this, "Some error. Scan Again!", Toast.LENGTH_SHORT).show();
                return;
            }else{
                receiptID = receiptId;
            }
        }

        String url = "http://101.53.157.211/api/receipt/search";

        Map<String, String> params = new HashMap<>();
        params.put("longitude", prefs.getString("longitude", "11.1111"));
        params.put("latitude", prefs.getString("latitude", "11.1111"));
        params.put("receipt_id", receiptID);

        Log.d(TAG, params.toString());
        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, parameters,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        mProgress.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(SearchReceiptActivity.this, obj.getString("statusDesc"));
                            }else{
                                Intent intent = new Intent(SearchReceiptActivity.this, ReceiptViewActivity.class);
                                intent.putExtra("receipt", obj.getString("data"));
                                startActivity(intent);

                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.dismiss();
                if(error.networkResponse == null){
                    Common.showNetworkError(SearchReceiptActivity.this, 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(SearchReceiptActivity.this, error.networkResponse.statusCode);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjReq);
    }

}
