package com.canvalabs.dharamkanta.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.canvalabs.dharamkanta.Adapters.GridViewCustomAdapter;

import com.canvalabs.dharamkanta.LoginActivity;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.WidgetViewHelper.ExpandableHeightGridView;
import com.canvalabs.dharamkanta.utils.Constants;

import java.util.ArrayList;

public class ProfileFragment extends Fragment {

    static private String TAG = "HomeFragment";
    SharedPreferences prefs;
    ArrayList<String> list = new ArrayList<>();
    ExpandableHeightGridView gridView;
    GridViewCustomAdapter grisViewCustomeAdapter;
    View view;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_profile, container, false);

        prefs = getContext().getSharedPreferences(Constants.PREF_NAME, getContext().MODE_PRIVATE);

        TextView logout = view.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

                getActivity().finish();
            }
        });

        try {
            TextView buildInfo = view.findViewById(R.id.buildVersion);
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            buildInfo.setText("Build v"+packageInfo.versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return view;
    }



}