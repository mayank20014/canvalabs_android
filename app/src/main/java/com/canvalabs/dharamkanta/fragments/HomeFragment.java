package com.canvalabs.dharamkanta.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.GridViewCustomAdapter;
import com.canvalabs.dharamkanta.Adapters.ViewPagerAdapter;
import com.canvalabs.dharamkanta.Model.CollectionObject;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.WhatsNext;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextView;
import com.canvalabs.dharamkanta.WidgetViewHelper.ExpandableHeightGridView;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;
import com.canvalabs.dharamkanta.utils.SliderUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {

    static private String TAG = "ReportFragment";
    SharedPreferences prefs;
    ArrayList<String> list = new ArrayList<>();
    ExpandableHeightGridView gridView;
    GridViewCustomAdapter grisViewCustomeAdapter;
    View view;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    TextView totalCollection;
    ArrayList<CollectionObject> gridViewList = new ArrayList<>();

    RequestQueue rq;
    List<SliderUtils> sliderImg;
    ViewPagerAdapter viewPagerAdapter;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;

    SwipeRefreshLayout pullToRefresh;
    ProgressDialog mProgress;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getContext().getSharedPreferences(Constants.PREF_NAME, getContext().MODE_PRIVATE);
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle("Refreshing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();
        refreshDashboardSummary();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);


        gridView = view.findViewById(R.id.gridViewCustom);
        totalCollection = view.findViewById(R.id.totalCollection);

        gridView.setNumColumns(4);
        gridView.setExpanded(true);
        grisViewCustomeAdapter = new GridViewCustomAdapter(getActivity(), gridViewList);
        gridView.setAdapter(grisViewCustomeAdapter);

        sliderImg = new ArrayList<>();

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        CustomTextView checkWhatsNext = (CustomTextView)view.findViewById(R.id.checkNext);
        checkWhatsNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), WhatsNext.class));
            }
        });

//        sliderDotspanel = (LinearLayout) view.findViewById(R.id.SliderDots);
        sendRequest();

        pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDashboardSummary();


            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

//                for (int i = 0; i < dotscount; i++) {
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.non_active_dot));
//                }
//
//                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }


        });
        return view;
    }


    private void refreshDashboardSummary() {

        String url = "http://101.53.157.211/api/user/dashboard";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            gridViewList.clear();

                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(getActivity(), obj.getString("statusDesc"));

                            }else{
                                JSONArray collections = obj.getJSONObject("data").getJSONArray("collections");
                                final int size = collections.length();
                                double totalAmount = 0;

                                for (int i = 0; i < size; i++)
                                {
                                    JSONObject a = collections.getJSONObject(i);
                                    CollectionObject o = new CollectionObject();
                                    if(a.getString("id").equalsIgnoreCase("null")){
                                        o.id = "1";
                                    }else{
                                        o.id = a.getString("id");
                                    }

                                    o.type_name = a.getString("type_name");
                                    o.icon_url = a.getString("icon_url");
                                    o.is_active = a.getString("is_active");
                                    o.count = a.getInt("count");
                                    o.amount = a.getDouble("collections");

                                    totalAmount += a.getDouble("collections");
                                    gridViewList.add(o);
                                }


                                grisViewCustomeAdapter.notifyDataSetChanged();

                                totalCollection.setText("\u20B9 "+ String.valueOf(totalAmount));
                                pullToRefresh.setRefreshing(false);

                            }
                            mProgress.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                pullToRefresh.setRefreshing(false);
                mProgress.dismiss();
                if(error.networkResponse == null){
                    Common.showNetworkError(getActivity(), 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(getActivity(), error.networkResponse.statusCode);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjReq);

    }
    public void sendRequest(){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.imagesSlider, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for(int i = 0; i < response.length(); i++){

                    SliderUtils sliderUtils = new SliderUtils();

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        sliderUtils.setSliderImageUrl(jsonObject.getString("image_url"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sliderImg.add(sliderUtils);

                }

                viewPagerAdapter = new ViewPagerAdapter( getActivity(), sliderImg);

                viewPager.setAdapter(viewPagerAdapter);
                final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (currentPage == viewPagerAdapter.getCount()-1) {
                            currentPage = 0;
                        }
                        viewPager.setCurrentItem(currentPage++, true);
                    }
                };

                timer = new Timer(); // This will create a new Thread
                timer.schedule(new TimerTask() { // task to be scheduled
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, DELAY_MS, PERIOD_MS);

//                dotscount = viewPagerAdapter.getCount();
//                dots = new ImageView[dotscount];
//
//                for(int i = 0; i < dotscount; i++){
//
//                    dots[i] = new ImageView(getActivity());
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.non_active_dot));
//
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//                    params.setMargins(8, 0, 8, 0);
//
//                    sliderDotspanel.addView(dots[i], params);
//
//                }
//
//                dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("hello",error.toString());
            }
        });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonArrayRequest);

    }
}