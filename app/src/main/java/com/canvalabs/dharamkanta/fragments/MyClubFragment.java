package com.canvalabs.dharamkanta.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.MyClubAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportListViewAdapter;
import com.canvalabs.dharamkanta.Listener.RecyclerItemClickListener;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.ReceiptViewActivity;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextInputEditText;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MyClubFragment extends Fragment {

    View view;
    private String TAG = "MyClubFragment";
    RecyclerView recyclerView;
    MyClubAdapter adapter;
    JSONArray collections;

    public MyClubFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_myclub, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        try {
            refreshReportList();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) throws JSONException {
                Toast.makeText(getActivity(), String.valueOf(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }

        }));

        return view;
    }



    private void refreshReportList() throws JSONException {

        String jsonResponse = "[{\"name\":\"PILOT216\",\"dailyInvestAmount\":200,\"OnRound\":7,\"totalRound\":10,\"status\":\"PENDING\",\"nextBidDate\":\"28-July-2019\"},{\"name\":\"PILOT216\",\"dailyInvestAmount\":200,\"OnRound\":7,\"totalRound\":10,\"status\":\"PENDING\",\"nextBidDate\":\"28-July-2019\"},{\"name\":\"PILOT216\",\"dailyInvestAmount\":200,\"OnRound\":7,\"totalRound\":10,\"status\":\"PENDING\",\"nextBidDate\":\"28-July-2019\"}]";
        collections = new JSONArray(jsonResponse);
        adapter = new MyClubAdapter(getActivity(), collections);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

    }
}