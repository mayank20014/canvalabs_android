package com.canvalabs.dharamkanta.fragments;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.canvalabs.dharamkanta.Adapters.GridViewCustomAdapter;
import com.canvalabs.dharamkanta.Model.CollectionObject;
import com.canvalabs.dharamkanta.PartyName;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.ReceiptViewActivity;
import com.canvalabs.dharamkanta.SearchReceiptActivity;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;
import com.canvalabs.dharamkanta.utils.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;


public class CaptureFirstWeightFragment extends Fragment {

    View view;
    static String measuredWeight = "0";
    TextView machineWeight;
    int TAKE_PHOTO_CODE = 0;
    int count = 0;
    Uri outputFileUri;
    int shoudlRunAPI = 1;
    SharedPreferences prefs;
    static String base64Image = "";
    RadioButton selected;
    EditText vehicle_number, material_name, party_name, party_contact, remarks, amount;
    Spinner vehicle_type;
    RadioGroup weight_type;
    ProgressDialog mProgress;

    public CaptureFirstWeightFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        shoudlRunAPI = 1;
        if(PartyName.status.equals("existing")){
            if(!PartyName.partydetails.equals("")){
        //            Toast.makeText(getActivity(), PartyName.partydetails, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject = new JSONObject(PartyName.partydetails);
                    party_name.setText(jsonObject.getString("party_name"));
                    party_contact.setText(jsonObject.getString("party_mobile"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else  if(PartyName.status.equals("new")){
            party_name.setText(PartyName.partydetails);
            party_contact.setText("");
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        shoudlRunAPI = 0;


    }

    public static CaptureFirstWeightFragment newInstance(String param1, String param2) {
        CaptureFirstWeightFragment fragment = new CaptureFirstWeightFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getActivity().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_capture, container, false);
        machineWeight = view.findViewById(R.id.measuredWeight);
        TextView capturePhoto = view.findViewById(R.id.capture_photo);
        capturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturePic();
            }
        });

        weight_type = view.findViewById(R.id.weight_type);
        vehicle_number = view.findViewById(R.id.vehicle_number);
        material_name = view.findViewById(R.id.material_name);
        party_name = view.findViewById(R.id.party_name);
        party_contact = view.findViewById(R.id.party_contact);
        vehicle_type = view.findViewById(R.id.vehicle_type);
        remarks = view.findViewById(R.id.remarks);
        amount = view.findViewById(R.id.amount);
        vehicle_type.setPrompt("Vehicle Type");
        party_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PartyName.class));
            }
        });

        Button btn_save = view.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveReceipt();
            }
        });

        getWeight();
//        final String partyDetails = getActivity().getIntent().getStringExtra("party_details");


//        PartyName activity = (PartyName) getActivity();
//        String myDataFromActivity = activity.getMyData();
//        if(myDataFromActivity!=""){
//            Toast.makeText(getActivity(), myDataFromActivity, Toast.LENGTH_SHORT).show();
//        }

        return view;
    }

    private void saveReceipt() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle("Processing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(true);
        mProgress.setIndeterminate(true);

        JSONObject params = new JSONObject();
        try {

            if(weight_type.getCheckedRadioButtonId() == 0){
                return;
            }

            selected = view.findViewById(weight_type.getCheckedRadioButtonId());
            if(selected == null){
                return;
            }
            if(!Validate.isValidVehicleNumber(vehicle_number.getText().toString())){
                Toast.makeText(getActivity(), "Please enter a valid vehicle number", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!Validate.isValidMobile(party_contact.getText().toString())){
                Toast.makeText(getActivity(), "Please enter a valid mobile number!", Toast.LENGTH_SHORT).show();
                return;
            }
            if(material_name.getText().toString().equals("")||party_contact.getText().toString().equals("")
                    || remarks.getText().toString().equals("") ||
                    amount.getText().toString().equals("")){
                Toast.makeText(getActivity(), "Please fill all the data!", Toast.LENGTH_SHORT).show();
                return;
            }

            params.put("weight_type", selected.getText().toString().toUpperCase());
            params.put("vehicle_number", vehicle_number.getText().toString());
            params.put("material_name", material_name.getText().toString());
            params.put("party_name", party_name.getText().toString());
            params.put("party_contact", party_contact.getText().toString());
            params.put("vehicle_type", vehicle_type.getSelectedItem().toString());
            params.put("remarks", remarks.getText().toString());
            params.put("amount", amount.getText().toString());
            params.put("captured_weight", "1024"); //measuredWeight);
            params.put("captured_image", base64Image);

            Iterator<String> iter = params.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    String value = params.getString(key);
                    // TODO: validate all keys else return
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

            String url = "http://101.53.157.211/api/receipt/captureFirstWeight";
            mProgress.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                    url, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            mProgress.dismiss();
//                            Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                if(!obj.getString("statusCode").equals("200")){
                                    Common.showDialogue(getContext(), obj.getString("statusDesc"));
                                }else{
                                    Intent intent = new Intent(getActivity(), ReceiptViewActivity.class);
                                    intent.putExtra("receipt", obj.getString("data"));
                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.dismiss();
                    if(error.networkResponse == null){
                        Common.showNetworkError(getContext(), 0);
                    }else if(error.networkResponse.statusCode != 200){
                        Common.showNetworkError(getContext(), error.networkResponse.statusCode);
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    String credentials = prefs.getString("app_token", "");
                    assert credentials != null;
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void capturePic(){

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        count++;
        String file = dir+count+".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
            Toast.makeText(getActivity(), "Some problem in creating file"+e, Toast.LENGTH_SHORT).show();
        }

        outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            Log.d("CameraDemo", "Pic saved");

            ContentResolver contentResolver = getActivity().getContentResolver();

            // Use the content resolver to open camera taken image input stream through image uri.
            InputStream inputStream = null;
            try {
                inputStream = contentResolver.openInputStream(outputFileUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // Decode the image input stream to a bitmap use BitmapFactory.
            Bitmap pictureBitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            pictureBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            base64Image = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

        }
    }
    
    private void getWeight() {

        if(shoudlRunAPI == 1){

            String url = "http://192.168.43.115:5000";

            StringRequest strReq = new StringRequest(Request.Method.GET,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(final String response) {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    machineWeight.setText(response+ " Kg");
                    measuredWeight = response;

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            getWeight();
                        }
                    }, 500);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            getWeight();
                        }
                    }, 5000);
                }
            });

            MySingleton.getInstance(getActivity()).addToRequestQueue(strReq);
        }

    }

}
