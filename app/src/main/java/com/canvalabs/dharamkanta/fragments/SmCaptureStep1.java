package com.canvalabs.dharamkanta.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.ReportAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportListViewAdapter;
import com.canvalabs.dharamkanta.Listener.RecyclerItemClickListener;
import com.canvalabs.dharamkanta.PartyName;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.ReceiptViewActivity;
import com.canvalabs.dharamkanta.SearchReceiptActivity;
import com.canvalabs.dharamkanta.SmCaptureStep2Activity;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextInputEditText;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;
import com.canvalabs.dharamkanta.utils.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class SmCaptureStep1 extends Fragment {

    View view;
    EditText vehicle_number, material_name, party_name, party_contact;

    public SmCaptureStep1() {
        // Required empty public constructor
    }
    @Override
    public void onResume() {
        super.onResume();

        if(PartyName.status.equals("existing")){
            if(!PartyName.partydetails.equals("")){
                //            Toast.makeText(getActivity(), PartyName.partydetails, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject = new JSONObject(PartyName.partydetails);
                    party_name.setText(jsonObject.getString("party_name"));
                    party_contact.setText(jsonObject.getString("party_mobile"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else  if(PartyName.status.equals("new")){
            party_name.setText(PartyName.partydetails.substring(9 , PartyName.partydetails.length()-2));
            party_contact.setText("");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sm_capture_step1, container, false);

        vehicle_number = view.findViewById(R.id.vehicle_number);
        party_contact = view.findViewById(R.id.party_contact);
        material_name = view.findViewById(R.id.material_name);
        party_name = view.findViewById(R.id.party_name);
        party_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PartyName.class));
            }
        });

        Button btn_next = view.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Validate.isValidVehicleNumber(vehicle_number.getText().toString())){
                    Toast.makeText(getContext(), "Please enter a valid vehicle number!", Toast.LENGTH_SHORT).show();
                    return ;
                }
                if (!Validate.isValidMobile(party_contact.getText().toString())){
                    Toast.makeText(getActivity(), "Please enter a valid party contact number!", Toast.LENGTH_SHORT).show();
                    return ;
                }
                if (material_name.getText().toString().equals("") || party_name.getText().toString().equals("") ){
                    Toast.makeText(getActivity(), "Please fill all the details!", Toast.LENGTH_SHORT).show();
                    return ;
                }
                Intent intent = new Intent(getActivity(), SmCaptureStep2Activity.class);
                intent.putExtra("material_name", material_name.getText().toString());
                intent.putExtra("vehicle_number", vehicle_number.getText().toString());
                intent.putExtra("party_name", party_name.getText().toString());
                intent.putExtra("party_contact", party_contact.getText().toString());
                startActivity(intent);
            }
        });
        return view;
    }

}