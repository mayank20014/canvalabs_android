package com.canvalabs.dharamkanta.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.canvalabs.dharamkanta.Adapters.GridViewCustomAdapter;
import com.canvalabs.dharamkanta.Model.CollectionObject;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.ReceiptViewActivity;
import com.canvalabs.dharamkanta.WidgetViewHelper.ExpandableHeightGridView;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationFragment extends Fragment {
    ProgressDialog mProgress;
    static private String TAG = "ConfigurationFragment";
    SharedPreferences prefs;
    EditText companyNameExitText, addrEditText, addr2EditText, printerStatusEditText, contactEditText, printerNameEditText, printerIPEditText, printerSRNoEditText;
    View view;

    public ConfigurationFragment() {
        // Required empty public constructor
    }

    public static ConfigurationFragment newInstance(String param1, String param2) {
        ConfigurationFragment fragment = new ConfigurationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getContext().getSharedPreferences(Constants.PREF_NAME, getContext().MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_config, container, false);
        companyNameExitText = view.findViewById(R.id.company_name);
        addrEditText = view.findViewById(R.id.address_line_1);
        addr2EditText = view.findViewById(R.id.address_line_2);
        contactEditText = view.findViewById(R.id.contact_details);

        printerNameEditText = view.findViewById(R.id.printer_name);
        printerIPEditText = view.findViewById(R.id.printer_ip);
        printerStatusEditText = view.findViewById(R.id.printer_status);
        printerSRNoEditText = view.findViewById(R.id.serial_number);

        Button btnRefreshPrintConf = view.findViewById(R.id.refreshPrinterConfig);
        btnRefreshPrintConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchWiFiPrinter();
            }
        });

        refreshConfig();

        final String IP = prefs.getString("PRINTER_IP", "");

        if(IP != null && !IP.isEmpty()) {
            printerIPEditText.setText(prefs.getString("PRINTER_IP", ""));
            printerNameEditText.setText(prefs.getString("PRINTER_MODELNAME", ""));
            printerSRNoEditText.setText(prefs.getString("PRINTER_SRNO", ""));
            printerStatusEditText.setText("ERROR_NONE");
        }


        return view;
    }

    private void refreshConfig() {
        mProgress = new ProgressDialog(getContext());
        mProgress.setTitle("Setting up Configurations...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();
        String url = "http://101.53.157.211/api/user/config";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
//                        mProgress.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(getContext(), obj.getString("statusDesc"));
                            }else{
//                                Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                                companyNameExitText.setText(obj.getJSONObject("data").getString("display_name"));
                                addrEditText.setText(obj.getJSONObject("data").getString("display_address_line1"));
                                addr2EditText.setText(obj.getJSONObject("data").getString("display_address_line2"));
                                contactEditText.setText(obj.getJSONObject("data").getString("communication_email"));
                            }

                            companyNameExitText.setEnabled(false);
                            addrEditText.setEnabled(false);
                            addr2EditText.setEnabled(false);
                            contactEditText.setEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mProgress.dismiss();
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                if(error.networkResponse == null){
                    Common.showNetworkError(getContext(), 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(getContext(), error.networkResponse.statusCode);
                }
                mProgress.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

    }

    void searchWiFiPrinter() {

        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle("Searching...");
        mProgress.setMessage("Please wait while we update printer settings...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

            JSONObject printerStatus = null;
            try {

                printerStatus = Common.scanPrinter();
                mProgress.dismiss();
                if(printerStatus == null){

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Printer not configured", Toast.LENGTH_SHORT).show();
                        }
                    });

                }else{

                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(Constants.PREF_NAME, getActivity().MODE_PRIVATE).edit();
                    editor.putString("PRINTER_IP", printerStatus.getString("ipAddress"));
                    editor.putString("PRINTER_MODELNAME", printerStatus.getString("modelName"));
                    editor.putString("PRINTER_SRNO", printerStatus.getString("serNo"));
                    editor.apply();

                    final JSONObject finalPrinterStatus = printerStatus;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                printerIPEditText.setText(finalPrinterStatus.getString("ipAddress"));
                                printerNameEditText.setText(finalPrinterStatus.getString("modelName"));
                                printerSRNoEditText.setText(finalPrinterStatus.getString("serNo"));
                                printerStatusEditText.setText("ERROR_NONE");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
                mProgress.dismiss();

            }

            }
        }).start();


    }


}