package com.canvalabs.dharamkanta.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.ReportAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportListViewAdapter;
import com.canvalabs.dharamkanta.Listener.RecyclerItemClickListener;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.ReceiptViewActivity;
import com.canvalabs.dharamkanta.SearchReceiptActivity;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextInputEditText;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextView;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ReportFragment extends Fragment {

    View view;
    ListView reportList;
    SharedPreferences prefs;
    ReportListViewAdapter reportListAdapter;
    private String TAG = "ReportFragment";
    RecyclerView recyclerView;
    ReportAdapter adapter;
    JSONArray collections;
//    SwipeRefreshLayout pullToRefresh;
    CustomTextInputEditText searchText;
    Button searchButton;
    ProgressDialog mProgress;
    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getContext().getSharedPreferences(Constants.PREF_NAME, getContext().MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_reports, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        searchText = view.findViewById(R.id.searchText);
        searchButton = view.findViewById(R.id.searchButton);
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle("Processing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();
        try {
            refreshReportList();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) throws JSONException {
                Intent intent = new Intent(getContext(), ReceiptViewActivity.class);
                intent.putExtra("receipt", collections.getJSONObject(position).toString());
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
//        pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
//        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                try {
//                    refreshReportList();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            private Timer timer=new Timer();
            private final long DELAY = 750;
            @Override
            public void afterTextChanged(Editable editable) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                try {if(searchText.getText().toString().equals("")){
                                    refreshReportList();
                                }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        DELAY
                );
            }
        });
       searchButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               try {
                   refreshReportList();
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       });
        return view;
    }



    private void refreshReportList() throws JSONException {
        final TextView layout = (TextView) view.findViewById(R.id.text);
        JSONObject params = new JSONObject();
        JSONObject searchParams = new JSONObject();
//        Map<String, String> params = new HashMap<>();
        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String formattedDate = df.format(c);
        params.put("start_date", "2019-04-01");
        params.put("end_date", formattedDate);
        params.put("latitude", prefs.getString("latitude", ""));
        params.put("longitude", prefs.getString("longitude", ""));

//            Log.e("inside report fragment", searchText.getText().toString());
            searchParams.put("value",searchText.getText().toString());
            searchParams.put("regex", "false");
            params.put("search",searchParams);




        String url = "http://101.53.157.211/api/analytics/weightRegister";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(getActivity(), obj.getString("statusDesc"));
                            }else{

                                if (obj.getJSONObject("data").getJSONArray("data").length()<=0) {
                                    layout.setVisibility(View.VISIBLE);
                                }else {
                                    layout.setVisibility(View.GONE);

                                }

                                collections = obj.getJSONObject("data").getJSONArray("data");
                                adapter = new ReportAdapter(getActivity(), collections);

                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(adapter);
                            }
                            mProgress.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("in report fragment",error.toString());
                //to hide the progressbar
//                pullToRefresh.setRefreshing(false);
                if(error.networkResponse == null){
                    Common.showNetworkError(getActivity(), 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(getActivity(), error.networkResponse.statusCode);
                }
                mProgress.dismiss();

            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjReq);

    }
}