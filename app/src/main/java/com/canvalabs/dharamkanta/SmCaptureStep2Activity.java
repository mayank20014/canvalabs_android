package com.canvalabs.dharamkanta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.canvalabs.dharamkanta.Adapters.SmCaptureStep2Adapter;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SmCaptureStep2Activity extends AppCompatActivity {

    SmCaptureStep2Adapter adapter;
    SharedPreferences prefs;
    JSONArray itemWeightList = new JSONArray();
    int shoudlRunAPI = 1;
    TextView machineWeight;
    EditText remarksEditText, amountEditText;
    String measuredWeight = "50";
    RecyclerView listView;
    Bundle intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sm_capture_step2);

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        listView = findViewById(R.id.newItemList);
        adapter = new SmCaptureStep2Adapter(SmCaptureStep2Activity.this, itemWeightList);
        listView.setAdapter(adapter);

        intent = getIntent().getExtras();
        machineWeight = findViewById(R.id.measuredWeight);

        remarksEditText = findViewById(R.id.remarks);
        amountEditText = findViewById(R.id.amount);

        Button btn_capture = findViewById(R.id.btn_capture);
        Button btn_save= findViewById(R.id.btn_save);

        btn_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject tmp = new JSONObject();
                try {
                    tmp.put("weight",measuredWeight);
                    tmp.put("itemName", intent.getString("material_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                itemWeightList.put(tmp);
                adapter.notifyDataSetChanged();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContinuousWeight();
            }
        });

        getWeight();

    }

    private void getWeight() {

        if(shoudlRunAPI == 1){

            String url = "http://192.168.43.115:5000";

            StringRequest strReq = new StringRequest(Request.Method.GET,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(final String response) {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    machineWeight.setText(response+ " Kg");
                    measuredWeight = response;

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            getWeight();
                        }
                    }, 500);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            getWeight();
                        }
                    }, 5000);
                }
            });

            MySingleton.getInstance(this).addToRequestQueue(strReq);
        }

    }


    public void saveContinuousWeight(){

        JSONObject param = new JSONObject();

        try {

            param.put("material_name", intent.getString("material_name"));
            param.put("party_name", intent.getString("party_name"));
            param.put("party_contact", intent.getString("party_contact"));
            param.put("remarks", remarksEditText.getText().toString());
            param.put("amount", amountEditText.getText().toString());
            param.put("continous_weight", itemWeightList); //measuredWeight);
            param.put("captured_image", "WW");

            Toast.makeText(this, param.toString(), Toast.LENGTH_SHORT).show();

            String url = "http://101.53.157.211/api/receipt/captureContinousWeight";

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                    url, param,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                if(!obj.getString("statusCode").equals("200")){
                                    Common.showDialogue(SmCaptureStep2Activity.this, obj.getString("statusDesc"));
                                }else{
                                    Intent intent = new Intent(SmCaptureStep2Activity.this, ReceiptViewActivity.class);
                                    intent.putExtra("receipt", obj.getString("data"));
                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {

                    if(error.networkResponse == null){
                        Common.showNetworkError(SmCaptureStep2Activity.this, 0);
                    }else if(error.networkResponse.statusCode != 200){
                        Common.showNetworkError(SmCaptureStep2Activity.this, error.networkResponse.statusCode);
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    String credentials = prefs.getString("app_token", "");
                    assert credentials != null;
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            MySingleton.getInstance(this).addToRequestQueue(jsonObjReq);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
