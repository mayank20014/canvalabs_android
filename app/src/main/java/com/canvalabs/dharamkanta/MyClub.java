package com.canvalabs.dharamkanta;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

import com.canvalabs.dharamkanta.fragments.HomeFragment;
import com.canvalabs.dharamkanta.fragments.MyClubFragment;

import java.util.ArrayList;

public class MyClub extends AppCompatActivity {

    ArrayList<Fragment> fragmentArrayList = new ArrayList<>();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(0);
                    return true;
                case R.id.navigation_dashboard:
                    loadFragment(0);
                    return true;
                case R.id.navigation_notifications:
                    loadFragment(0);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myclub);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentArrayList.add(new MyClubFragment());
        loadFragment(0);
    }

    public void loadFragment(int fragmentIndex) {
        // load fragment
        FragmentManager manager =getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment tmp = fragmentArrayList.get(fragmentIndex);

        for (int i = 0; i < fragmentArrayList.size(); i++){
            transaction.hide(fragmentArrayList.get(i));
        }

        if(tmp.isAdded()){
            transaction.show(tmp);
        }else{
            transaction.add(R.id.frame_container, tmp).show(tmp);
        }
        transaction.commit();
    }

}
