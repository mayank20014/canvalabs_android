package com.canvalabs.dharamkanta;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.canvalabs.dharamkanta.fragments.CaptureFirstWeightFragment;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CaptureSecondWeight extends AppCompatActivity {

    SharedPreferences prefs;
    private String TAG = "ReceiptViewActivity";
    TextView measuredWeight;
    int TAKE_PHOTO_CODE = 0;
    public static int count = 0;
    Uri outputFileUri;
    ImageButton captureImage;
    JSONObject r;
    String weight_type = "";
    String base64Image =  "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturesecondweight);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        final String receipt = getIntent().getStringExtra("receipt");
        if(receipt == null || receipt.isEmpty()){
            Common.showNetworkError(CaptureSecondWeight.this, 0);
            finish();
        }

        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button btn_update = findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateReceipt();
            }
        });

        captureImage = findViewById(R.id.capture_photo);
        TextView vehicle_number = findViewById(R.id.vehicle_number);
        TextView tare_weight = findViewById(R.id.tare_weight);
        TextView tare_weight_dt = findViewById(R.id.tare_weight_dt);
        TextView gross_weight = findViewById(R.id.gross_weight);
        TextView gross_weight_dt = findViewById(R.id.gross_weight_dt);
        TextView party_name = findViewById(R.id.party_name);
        TextView party_contact = findViewById(R.id.party_contact);
        measuredWeight = findViewById(R.id.measuredWeight);

        try {
            r = new JSONObject(receipt);

            String tare_wt = r.getString("tare_weight");
            String tare_wt_dt = r.getString("tare_weight_measure_dt");
            String gross_wt = r.getString("gross_weight");
            String gross_wt_dt = r.getString("gross_weight_measure_dt");
            String net_wt = "NA";

            if(gross_wt.isEmpty() || gross_wt.equalsIgnoreCase("null") || Double.valueOf(gross_wt) == 0){
                gross_wt = "NA";
                gross_wt_dt = "NA";
                btn_update.setText("Capture Gross WT");
                weight_type = "GROSS";
            }

            if(tare_wt.isEmpty() || tare_wt.equalsIgnoreCase("null")  || Double.valueOf(tare_wt) == 0) {
                tare_wt = "NA";
                tare_wt_dt = "NA";
                btn_update.setText("Capture Tare WT");
                weight_type = "TARE";
            }

            vehicle_number.setText(r.getString("vehicle_number"));
            tare_weight.setText(tare_wt);
            tare_weight_dt.setText(tare_wt_dt);
            gross_weight.setText(gross_wt);
            gross_weight_dt.setText(gross_wt_dt);
            party_name.setText(r.getString("carrier_name"));
            party_contact.setText(r.getString("alert_mobile_email"));

            getWeight();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updateReceipt() {
        JSONObject params = new JSONObject();
        try {

            params.put("weight_type", weight_type);
            params.put("receipt_id", r.getString("id"));
            params.put("captured_weight", "1024"); //measuredWeight);
            params.put("captured_image", base64Image);

            Iterator<String> iter = params.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    String value = params.getString(key);
                    // TODO: validate all keys else return
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

            String url = "http://101.53.157.211/api/receipt/captureSecondWeight";

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                    url, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                if(!obj.getString("statusCode").equals("200")){
                                    Common.showDialogue(CaptureSecondWeight.this, obj.getString("statusDesc"));
                                }else{
                                    Intent intent = new Intent(CaptureSecondWeight.this, ReceiptViewActivity.class);
                                    intent.putExtra("receipt", obj.getString("data"));
                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {

                    if(error.networkResponse == null){
                        Common.showNetworkError(CaptureSecondWeight.this, 0);
                    }else if(error.networkResponse.statusCode != 200){
                        Common.showNetworkError(CaptureSecondWeight.this, error.networkResponse.statusCode);
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    String credentials = prefs.getString("app_token", "");
                    assert credentials != null;
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            MySingleton.getInstance(CaptureSecondWeight.this).addToRequestQueue(jsonObjReq);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void capturePic(View v){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        count++;
        String file = dir+count+".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
            Toast.makeText(this, "Some problem in creating file"+e, Toast.LENGTH_SHORT).show();
        }

        outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            Log.d("CameraDemo", "Pic saved");

            ContentResolver contentResolver = this.getContentResolver();

            // Use the content resolver to open camera taken image input stream through image uri.
            InputStream inputStream = null;
            try {
                inputStream = contentResolver.openInputStream(outputFileUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // Decode the image input stream to a bitmap use BitmapFactory.
            Bitmap pictureBitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            pictureBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            base64Image = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        }
    }

    private void getWeight() {

        String url = "http://192.168.43.115:5000";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {
                //Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                measuredWeight.setText(response+ " Kg");

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        getWeight();
                    }
                }, 500);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        getWeight();
                    }
                }, 5000);
            }
        });

        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }


}
