package com.canvalabs.dharamkanta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import android.widget.ImageButton;

import com.canvalabs.dharamkanta.WidgetViewHelper.ExpandableHeightGridView;
import com.canvalabs.dharamkanta.fragments.ConfigurationFragment;
import com.canvalabs.dharamkanta.fragments.ReportFragment;
import com.canvalabs.dharamkanta.fragments.HomeFragment;
import com.canvalabs.dharamkanta.fragments.CaptureFirstWeightFragment;
import com.canvalabs.dharamkanta.fragments.ProfileFragment;
import com.canvalabs.dharamkanta.fragments.SmCaptureStep1;
import com.canvalabs.dharamkanta.utils.Constants;

import java.util.ArrayList;


public class WelcomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static private String TAG = "WelcomeActivity";
    SharedPreferences prefs;
    private RecyclerView recyclerView;
    ArrayList<String> list = new ArrayList<>();
    ExpandableHeightGridView gridView;
    ArrayList<Fragment> fragmentArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView navView = findViewById(R.id.nav_view_bottom);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ImageButton searchBtn = findViewById(R.id.toolbar_search);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchReceiptActivity.class);
                startActivity(intent);
            }
        });

        fragmentArrayList.add(new HomeFragment());
        fragmentArrayList.add(new ReportFragment());
        fragmentArrayList.add(new CaptureFirstWeightFragment());
        fragmentArrayList.add(new ConfigurationFragment());
        fragmentArrayList.add(new ProfileFragment());
        fragmentArrayList.add(new SmCaptureStep1());

        loadFragment(0);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.navigation_home:
                loadFragment(0);
                return true;
            case R.id.navigation_dashboard:
                loadFragment(1);
                return true;
            case R.id.navigation_notifications:
                if(prefs.getString("company_type", "").equals("DHARAMKANTA")){
                    loadFragment(2);
                }else{
                    loadFragment(5);
                }
                return true;
            case R.id.navigation_config:
                loadFragment(3);
                return true;
            case R.id.navigation_profile:
                loadFragment(4);
                return true;
        }
        return false;
        }
    };

    public void loadFragment(int fragmentIndex) {
        // load fragment
        FragmentManager manager =getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment tmp = fragmentArrayList.get(fragmentIndex);

        for (int i = 0; i < fragmentArrayList.size(); i++){
            transaction.hide(fragmentArrayList.get(i));
        }

        if(tmp.isAdded()){
            transaction.show(tmp);
        }else{
            transaction.add(R.id.frame_container, tmp).show(tmp);
        }
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.reports:
                startActivity(new Intent(this, ReportsActivity.class));

        }

        return false;
    }
}
