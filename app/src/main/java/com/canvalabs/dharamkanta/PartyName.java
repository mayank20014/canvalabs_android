package com.canvalabs.dharamkanta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.PartyNameAdapter;
import com.canvalabs.dharamkanta.Model.CollectionObject;
import com.canvalabs.dharamkanta.fragments.CaptureFirstWeightFragment;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.security.AccessController.getContext;

public class PartyName extends AppCompatActivity {

    ListView lv;
    SharedPreferences prefs;

    public static String partydetails = "";
    public static String status = "";

    EditText inputSearch;
    JSONObject partyDetails;
    public ArrayList<JSONObject> arrayList = new ArrayList<>();
    PartyNameAdapter adapter;
    JSONArray parties;
    TextView newParty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_name);

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        lv = findViewById(R.id.party_list);
        inputSearch = findViewById(R.id.inputSearch);

        adapter =new PartyNameAdapter(PartyName.this,arrayList);
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);

        newParty = findViewById(R.id.addNewParty);
        newParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                partydetails = inputSearch.getText().toString();
                status = "new";
                onBackPressed();
            }
        });

        enableSearchFunctionality();
        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    partydetails = adapter.list.get(position).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                status = "existing";
                onBackPressed();
            }});

        refreshParties();

    }

    public void refreshParties(){

        String url = "http://101.53.157.211/api/master/party";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(PartyName.this, obj.getString("statusDesc"));

                            }else{

                                parties = obj.getJSONArray("data");
                                for (int i = 0; i < parties.length(); i++) {

                                    arrayList.add(parties.getJSONObject(i));

                                }

                                adapter.notifyDataSetChanged();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                if(error.networkResponse == null){
                    Common.showNetworkError(PartyName.this, 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(PartyName.this, error.networkResponse.statusCode);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjReq);

    }

    private void enableSearchFunctionality(){
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                newParty.setText(Html.fromHtml("CREATE &ldquo;" + charSequence + "&rdquo;"));
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

}
