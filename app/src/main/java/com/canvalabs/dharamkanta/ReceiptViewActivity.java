package com.canvalabs.dharamkanta;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class ReceiptViewActivity extends AppCompatActivity {

    SharedPreferences prefs;
    private String TAG = "ReceiptViewActivity";
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        final String receipt = getIntent().getStringExtra("receipt");
        if(receipt == null || receipt.isEmpty()){
            Common.showNetworkError(ReceiptViewActivity.this, 0);
            finish();
        }

        TextView receipt_id = findViewById(R.id.receipt_id);
        TextView vehicle_number = findViewById(R.id.vehicle_number);
        TextView material_name = findViewById(R.id.material_name);
        TextView tare_weight = findViewById(R.id.tare_weight);
        TextView tare_weight_dt = findViewById(R.id.tare_weight_dt);
        TextView gross_weight = findViewById(R.id.gross_weight);
        TextView gross_weight_dt = findViewById(R.id.gross_weight_dt);
        TextView net_weight = findViewById(R.id.net_weight);
        TextView party_name = findViewById(R.id.party_name);
        TextView party_contact = findViewById(R.id.party_contact);
        TextView amount = findViewById(R.id.amount);
        TextView remarks = findViewById(R.id.remarks);


        Button btn_capture = findViewById(R.id.btn_capture);
        btn_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReceiptViewActivity.this, CaptureSecondWeight.class);
                intent.putExtra("receipt",receipt);
                startActivity(intent);
                finish();
            }
        });


        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            final JSONObject r = new JSONObject(receipt);

            Button btn_print = findViewById(R.id.btn_print);
            btn_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("application/pdf");
                    startActivityForResult(intent, 1);
                }
            });

            try{

                String uri = "@drawable/vehicle_"+r.getString("id");
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                ImageView i = findViewById(R.id.vehicle_type_img);
                i.setImageDrawable(res);

            }catch (Resources.NotFoundException e){
                e.printStackTrace();
            }


            String tare_wt = r.getString("tare_weight");
            String tare_wt_dt = r.getString("tare_weight_measure_dt");
            String gross_wt = r.getString("gross_weight");
            String gross_wt_dt = r.getString("gross_weight_measure_dt");
            String net_wt = "NA";

            if(gross_wt.isEmpty() || gross_wt.equalsIgnoreCase("null") || Double.valueOf(gross_wt) == 0){
                gross_wt = "NA";
                gross_wt_dt = "NA";
                btn_capture.setText("Capture Gross WT");
            }

            if(tare_wt.isEmpty() || tare_wt.equalsIgnoreCase("null")  || Double.valueOf(tare_wt) == 0) {
                tare_wt = "NA";
                tare_wt_dt = "NA";
                btn_capture.setText("Capture Tare WT");
            }

            if(!gross_wt.equals("NA") && !tare_wt.equals("NA")){
                net_wt = String.valueOf(Double.valueOf(gross_wt) - Double.valueOf(tare_wt))+ " Kg";
                btn_capture.setVisibility(View.GONE);
            }else{
                net_wt = "NA";
            }

            receipt_id.setText("Receipt Details #"+r.getString("id"));
            vehicle_number.setText(r.getString("vehicle_number"));
            material_name.setText(r.getString("material_name"));
            tare_weight.setText(tare_wt);
            tare_weight_dt.setText(tare_wt_dt);
            gross_weight.setText(gross_wt);
            gross_weight_dt.setText(gross_wt_dt);
            net_weight.setText(net_wt);
            party_name.setText(r.getString("carrier_name"));
            party_contact.setText(r.getString("alert_mobile_email"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void printPDF(final String path) {

        final String IP = prefs.getString("PRINTER_IP", "");

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Searching...");
        mProgress.setMessage("Please wait while we update printer settings...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();

        if(IP == null || IP.isEmpty()){

            new Thread(new Runnable() {
                @Override
                public void run() {

                    JSONObject printerStatus = null;
                    try {

                        printerStatus = Common.scanPrinter();
                        if(printerStatus == null){

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mProgress.dismiss();
                                    Toast.makeText(ReceiptViewActivity.this, "Printer not configured", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }else{

                            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE).edit();
                            editor.putString("PRINTER_IP", printerStatus.getString("ipAddress"));
                            editor.putString("PRINTER_MODELNAME", printerStatus.getString("modelName"));
                            editor.putString("PRINTER_SRNO", printerStatus.getString("serNo"));
                            editor.apply();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mProgress.dismiss();
                                }
                            });

                            printPDF(path);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).start();

            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgress.setTitle("Printing");
                mProgress.setMessage("Please wait...");
                Toast.makeText(getApplicationContext(), IP, Toast.LENGTH_SHORT).show();
            }
        });

        final Printer printer = new Printer();
        PrinterInfo settings = printer.getPrinterInfo();
        settings.printerModel = PrinterInfo.Model.QL_720NW;
        settings.port = PrinterInfo.Port.NET;
        settings.ipAddress = IP;
        settings.labelNameIndex = 15; //LabelInfo.QL700.W29.ordinal();
        settings.printMode = PrinterInfo.PrintMode.FIT_TO_PAGE;
        settings.isAutoCut = false;

        printer.setPrinterInfo(settings);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (printer.startCommunication()) {

                    final PrinterStatus result = printer.printPdfFile(path, 1);
                    if (result.errorCode != PrinterInfo.ErrorCode.ERROR_NONE) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgress.setTitle("Error Occurred");
                                mProgress.setMessage(result.errorCode.toString());
                            }
                        });
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgress.dismiss();
                        }
                    });
                    printer.endCommunication();
                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file

                    Uri uri = data.getData();
                    String fileName = getFileName(uri);

                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
                    printPDF(file.getAbsolutePath());
                }

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


}
