package com.canvalabs.dharamkanta;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.Adapters.PartyNameAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportAdapter;
import com.canvalabs.dharamkanta.Adapters.ReportSettingsAdapter;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static java.security.AccessController.getContext;

public class ReportsActivity extends AppCompatActivity {
    ReportSettingsAdapter adapter;
    public ArrayList<JSONObject> arrayList = new ArrayList<>();
    JSONArray reports;
    ListView lv;
    SharedPreferences prefs;
    ProgressDialog mProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        lv= findViewById(R.id.report_list);

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Refreshing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();
        try {
            refreshReport();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void refreshReport() throws JSONException {

        String url = "http://101.53.157.211/api/user/available_reports";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        mProgress.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(getApplicationContext(), obj.getString("statusDesc"));
                            }else{

                                reports = obj.getJSONArray("data");
                                for (int i = 0; i < reports.length(); i++) {


                                    arrayList.add(reports.getJSONObject(i));
                                }
                                adapter =new ReportSettingsAdapter(ReportsActivity.this,arrayList);
                                lv.setAdapter(adapter);


                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("in report fragment",error.toString());
                //to hide the progressbar
//                pullToRefresh.setRefreshing(false);
                mProgress.dismiss();

                if(error.networkResponse == null){
                    Common.showNetworkError(getApplicationContext(), 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(getApplicationContext(), error.networkResponse.statusCode);
                }
//                mProgress.dismiss();

            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq);

    }
}
