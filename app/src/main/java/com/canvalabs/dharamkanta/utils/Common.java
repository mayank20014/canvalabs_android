package com.canvalabs.dharamkanta.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.canvalabs.dharamkanta.ErrorShowActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class Common {


    public static void showDialogue(final Context context, String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public static void showNetworkError(Context context, int statusCode) {

        Intent intent = new Intent(context, ErrorShowActivity.class);

        String errorMessage;

        switch (statusCode){
            case 0:
                errorMessage = "Please restart the device and try again";
                break;
            case 401:
                errorMessage = "Please enter correct credentials";
                break;
            default:
                errorMessage = "Please contact helpdesk #"+ statusCode;
                break;
        }

        intent.putExtra("errorMessage", errorMessage);

        context.startActivity(intent);

    }

    public static JSONObject scanPrinter() throws JSONException {
        Printer printer = new Printer();
        NetPrinter[] printerList = printer.getNetPrinters("QL-720NW");
        JSONObject resp = null;

        for (final NetPrinter device: printerList) {

            resp = new JSONObject();
            resp.put("ipAddress",device.ipAddress);
            resp.put("modelName",device.modelName);
            resp.put("serNo",device.serNo);

        }

        return resp;
    }

}
