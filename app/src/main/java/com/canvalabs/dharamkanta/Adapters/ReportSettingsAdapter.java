package com.canvalabs.dharamkanta.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.canvalabs.dharamkanta.R;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextView;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReportSettingsAdapter extends ArrayAdapter<JSONObject> implements Filterable {


    public ArrayList<JSONObject> list;
    private Context context;
    int setState = 0;
    SharedPreferences prefs;
    int reportId;
    SwitchCompat reportToggle;
    public ReportSettingsAdapter(Context context, ArrayList<JSONObject> list) {

        super(context, 0);
        this.context = context;
        this.list = list;


    }

    public View getView(int position, View convertView, ViewGroup parent) {


        View row = convertView;

//        if (row == null) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(R.layout.report_list_item, parent, false);
        prefs = getContext().getSharedPreferences(Constants.PREF_NAME, getContext().MODE_PRIVATE);
        try {

            CustomTextView reportName = row.findViewById(R.id.report_name);
            CustomTextView reportText = row.findViewById(R.id.report_text);
             reportToggle = row.findViewById(R.id.report_toggle);
//            Toast.makeText(context,list.get(position).getString("report_name") , Toast.LENGTH_SHORT).show();
            reportName.setText(list.get(position).getString("report_name"));
            reportText.setText(list.get(position).getString("report_desc"));
            reportId = Integer.parseInt(list.get(position).getString("id"));
            final int post = position;
            if(Integer.parseInt(list.get(position).getString("is_active"))==1){
                reportToggle.toggle();
            }
            reportToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked){
                        setState = 1;
                    }else{
                        setState = 0;
                    }


                    try {
                        refreshReportList(Integer.parseInt(list.get(post).getString("id")), isChecked);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });



        } catch (JSONException e) {
            e.printStackTrace();
        }


//        }

        return row;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    private void refreshReportList(int reportid, boolean isChecked) throws JSONException {

        JSONObject params = new JSONObject();

        params.put("report_id", reportid );
        params.put("set_state", isChecked);
        String url = "http://101.53.157.211/api/user/toggle_report";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
                                Common.showDialogue(getContext(), obj.getString("statusDesc"));
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("in report fragment",error.toString());
                //to hide the progressbar
//                pullToRefresh.setRefreshing(false);
                if(error.networkResponse == null){
                    Common.showNetworkError(getContext(), 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(getContext(), error.networkResponse.statusCode);
                }


            }
        }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = prefs.getString("app_token", "");
                assert credentials != null;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

    }



}