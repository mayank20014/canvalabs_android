package com.canvalabs.dharamkanta.Adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.canvalabs.dharamkanta.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {

    private JSONArray collections;
    private Context context;
    private ArrayList<String> names;

    public ReportAdapter(Context context, JSONArray collections) {
        this.context = context;
        this.collections = collections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.listitem_report_fragment, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            JSONObject r = collections.getJSONObject(position);

            if(r.getString("weight_type").equals("GROSSTARE")){
                holder.continous.setVisibility(View.GONE);
                holder.dharamkanta.setVisibility(View.VISIBLE);
                String tare_wt = r.getString("tare_weight");
                String tare_wt_dt = r.getString("tare_weight_measure_dt");
                String gross_wt = r.getString("gross_weight");
                String gross_wt_dt = r.getString("gross_weight_measure_dt");
                String net_wt = "NA";

                if(gross_wt.isEmpty() || gross_wt.equalsIgnoreCase("null") || Double.valueOf(gross_wt) == 0){
                    gross_wt = "NA";
                    gross_wt_dt = "NA";
                    holder.statusIndicactor.setBackgroundColor(Color.parseColor("#fbbc05"));
                }

                if(tare_wt.isEmpty() || tare_wt.equalsIgnoreCase("null")  || Double.valueOf(tare_wt) == 0) {
                    tare_wt = "NA";
                    tare_wt_dt = "NA";
                    holder.statusIndicactor.setBackgroundColor(Color.parseColor("#4184f3"));
                }

                if(!gross_wt.equals("NA") && !tare_wt.equals("NA")){
                    net_wt = String.valueOf(Double.valueOf(gross_wt) - Double.valueOf(tare_wt))+ " Kg";
                    holder.statusIndicactor.setBackgroundColor(Color.parseColor("#34a853"));
                }else{
                    net_wt = "NA";
                }

                holder.vehicle_details.setText(r.getString("vehicle_number") + " ("+ r.getString("vehicle_type_name") +")");
                holder.tare_weight.setText(tare_wt);
                holder.gross_weight.setText(gross_wt);
                holder.net_weight.setText(net_wt);
                holder.tare_weight_dt.setText(tare_wt_dt);
                holder.gross_weight_dt.setText(gross_wt_dt);

            }else{
                holder.dharamkanta.setVisibility(View.GONE);
                holder.continous.setVisibility(View.VISIBLE);
                String net_wt = r.getString("net_weight");
                String net_wt_dt = r.getString("net_weight_measure_dt");

                if(net_wt.isEmpty() || net_wt.equalsIgnoreCase("null") || Double.valueOf(net_wt) == 0){
                    net_wt = "NA";
                    net_wt_dt = "NA";
                }


                holder.cont_material_name.setText(r.getString("material_name"));
                holder.cont_net_weight.setText(net_wt);
                holder.cont_net_weight_measure_dt.setText(net_wt_dt);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return collections.length();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView vehicle_details, tare_weight, gross_weight, tare_weight_dt, gross_weight_dt, net_weight ;
        TextView cont_item_count, cont_material_name, cont_net_weight, cont_net_weight_measure_dt;
        CardView dharamkanta, continous;

        ImageView statusIndicactor;
        Button print;

        ViewHolder(View itemView) {
            super(itemView);

            vehicle_details = itemView.findViewById(R.id.vehicle_details);
            tare_weight = itemView.findViewById(R.id.tare_weight);
            gross_weight = itemView.findViewById(R.id.gross_weight);
            tare_weight_dt = itemView.findViewById(R.id.tare_weight_dt);
            gross_weight_dt = itemView.findViewById(R.id.gross_weight_dt);
            net_weight = itemView.findViewById(R.id.net_weight);
            statusIndicactor = itemView.findViewById(R.id.statusIndicactor);
            dharamkanta = itemView.findViewById(R.id.DHARAMKANTA);

            cont_item_count = itemView.findViewById(R.id.cont_item_count);
            cont_material_name = itemView.findViewById(R.id.cont_material_name);
            cont_net_weight = itemView.findViewById(R.id.cont_net_weight);
            cont_net_weight_measure_dt = itemView.findViewById(R.id.cont_net_weight_measure_dt);
            continous = itemView.findViewById(R.id.CONTINOUS);

        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}