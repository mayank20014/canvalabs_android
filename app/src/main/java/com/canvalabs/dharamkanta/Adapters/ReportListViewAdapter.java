package com.canvalabs.dharamkanta.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.canvalabs.dharamkanta.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportListViewAdapter extends ArrayAdapter
{
    Context context;
    JSONArray arrayList;

    public ReportListViewAdapter(Context context, JSONArray arrayList)
    {
        super(context, 0);
        this.context=context;
        this.arrayList=arrayList;
    }

    public int getCount()
    {
        return arrayList.length();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.listitem_grid_dashboard, parent, false);

            try {
                JSONObject a = arrayList.getJSONObject(position);
                TextView textViewTitle = (TextView) row.findViewById(R.id.textView);
                textViewTitle.setText(a.getString("id"));

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        return row;

    }
}