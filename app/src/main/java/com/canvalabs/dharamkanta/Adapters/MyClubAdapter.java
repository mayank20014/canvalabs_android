package com.canvalabs.dharamkanta.Adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.canvalabs.dharamkanta.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyClubAdapter extends RecyclerView.Adapter<MyClubAdapter.ViewHolder> {

    private JSONArray collections;
    private Context context;
    private ArrayList<String> names;

    public MyClubAdapter(Context context, JSONArray collections) {
        this.context = context;
        this.collections = collections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.listitem_myclub_fragment, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            JSONObject r = collections.getJSONObject(position);

                String name = r.getString("name");
                String dailyInvestAmount = r.getString("dailyInvestAmount");
                String OnRound = r.getString("OnRound");
                String status = r.getString("status");
                String nextBidDate = r.getString("nextBidDate");

                holder.name.setText(name);
                holder.dailyInvestAmount.setText(dailyInvestAmount);
                holder.roundDetails.setText(OnRound);
                holder.status.setText(status);
                holder.nextBidDate.setText(nextBidDate);



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return collections.length();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, dailyInvestAmount, roundDetails, status, nextBidDate ;
        Button print;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            dailyInvestAmount = itemView.findViewById(R.id.dailyInvestAmount);
            roundDetails = itemView.findViewById(R.id.roundDetails);
            status = itemView.findViewById(R.id.status);
            nextBidDate = itemView.findViewById(R.id.nextBidDate);

        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}