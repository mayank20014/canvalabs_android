package com.canvalabs.dharamkanta.Adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.canvalabs.dharamkanta.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SmCaptureStep2Adapter extends RecyclerView.Adapter<SmCaptureStep2Adapter.ViewHolder> {
    private JSONArray collections;

    private Context context;

    public SmCaptureStep2Adapter(Context context, JSONArray collections) {
        this.context = context;
        this.collections = collections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.listitem_sm_capture_step2, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            JSONObject r = (JSONObject) collections.get(position);
            Log.d("position", r.toString());

            holder.material_name.setText(r.getString("itemName")+ " #"+String.valueOf(position+1));
            holder.measuredWeight.setText(r.getString("weight") + " Kg");
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collections.remove(position);
                    notifyDataSetChanged();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return collections.length();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView material_name, measuredWeight ;
        ImageButton delete;

        ViewHolder(View itemView) {
            super(itemView);

            material_name = itemView.findViewById(R.id.material_name);
            measuredWeight = itemView.findViewById(R.id.measuredWeight);
            delete = itemView.findViewById(R.id.delete);
        }
    }

}