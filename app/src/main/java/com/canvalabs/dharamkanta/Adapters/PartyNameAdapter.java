package com.canvalabs.dharamkanta.Adapters;
import android.app.Activity;
import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;

import android.view.ViewGroup;

import android.widget.ArrayAdapter;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.canvalabs.dharamkanta.R;

import org.json.JSONArray;
import org.json.JSONException;

import org.json.JSONObject;

import java.util.ArrayList;

public class PartyNameAdapter extends ArrayAdapter<JSONObject> implements Filterable {

    ArrayList<JSONObject> originalList;
    public ArrayList<JSONObject> list;
    private Context context;
    private PartyFilter filter;


    public PartyNameAdapter(Context context, ArrayList<JSONObject> list) {

        super(context, 0);
        this.context = context;
        this.list = list;
        this.originalList = list;

    }

    public View getView(int position, View convertView, ViewGroup parent) {


        View row = convertView;

//        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.party_list_item, parent, false);

            try {

                TextView partyName = row.findViewById(R.id.name);
                TextView partyNumber = row.findViewById(R.id.number);
                partyName.setText(list.get(position).getString("party_name"));
                partyNumber.setText(list.get(position).getString("party_mobile"));

            } catch (JSONException e) {
                e.printStackTrace();
            }


//        }

        return row;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {

        if(filter == null)
            filter = new PartyFilter();
        return filter;
    }

   private class PartyFilter extends Filter {

       @Override
       protected FilterResults performFiltering(CharSequence constraint) {

           constraint = constraint.toString().toLowerCase();

           FilterResults newFilterResults = new FilterResults();

           if (constraint.length() > 0) {


               ArrayList<JSONObject> auxData = new ArrayList<JSONObject>();

               for (int i = 0; i < originalList.size(); i++) {

                   try {
                       if (originalList.get(i).getString("party_name").toLowerCase().contains(constraint) ||
                               originalList.get(i).getString("party_mobile").toLowerCase().contains(constraint) )
                           auxData.add(originalList.get(i));
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }

               newFilterResults.count = auxData.size();
               newFilterResults.values = auxData;
           } else {

               newFilterResults.count = originalList.size();
               newFilterResults.values = originalList;
           }

           return newFilterResults;
       }

       @Override
       protected void publishResults(CharSequence constraint, FilterResults results) {

           ArrayList<JSONObject> arrayList = new ArrayList();
           try {
               JSONArray a = new JSONArray(results.values.toString());

               for (int i = 0; i < a.length(); i++) {
                   arrayList.add(a.getJSONObject(i));
               }
           } catch (JSONException e) {
               e.printStackTrace();
           }

           list = arrayList;
           notifyDataSetChanged();
       }
    }
}