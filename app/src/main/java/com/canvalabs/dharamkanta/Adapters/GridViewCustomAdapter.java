package com.canvalabs.dharamkanta.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.canvalabs.dharamkanta.Model.CollectionObject;
import com.canvalabs.dharamkanta.R;

import java.util.ArrayList;

public class GridViewCustomAdapter extends ArrayAdapter
{
    Context context;
    ArrayList arrayList;

    public GridViewCustomAdapter(Context context, ArrayList arrayList)
    {
        super(context, 0);
        this.context=context;
        this.arrayList=arrayList;
    }

    public int getCount()
    {
        return arrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.listitem_grid_dashboard, parent, false);

            CollectionObject a = (CollectionObject) arrayList.get(position);

            String uri = "@drawable/vehicle_"+a.getId();

            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());

                Log.d("ERRORTAG", uri);

            Drawable res = context.getResources().getDrawable(imageResource);

            TextView textViewTitle = (TextView) row.findViewById(R.id.textView);
            ImageView imageViewIte = (ImageView) row.findViewById(R.id.imageView);

            textViewTitle.setText("\u20B9"+String.valueOf(a.getAmount()));
            imageViewIte.setImageDrawable(res);
        }

        return row;

    }

}