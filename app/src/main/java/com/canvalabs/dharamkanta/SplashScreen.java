package com.canvalabs.dharamkanta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.PermissionHelper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

public class SplashScreen extends AppCompatActivity {

	private static int SPLASH_TIME_OUT = 3000;
	private PermissionHelper permissionHelper;
	private boolean isPermissionGranted = false;
	private GoogleApiClient googleApiClient;
	private LocationRequest locationRequest;
	private Location location;
	SharedPreferences prefs;
	private String TAG = "SplashScreen";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
		String user_id = prefs.getString("user_id", null);
		if (user_id != null) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {

					Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
					startActivity(i);

					// close this activity
					finish();
				}
			}, 0);

			return;
		}

		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				Intent i = new Intent(SplashScreen.this, LoginActivity.class);
				startActivity(i);

				// close this activity
				finish();
			}
		}, 2000);

	}
}