package com.canvalabs.dharamkanta;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.canvalabs.dharamkanta.WidgetViewHelper.CustomTextView;
import com.canvalabs.dharamkanta.utils.Common;
import com.canvalabs.dharamkanta.utils.Constants;
import com.canvalabs.dharamkanta.utils.MySingleton;
import com.canvalabs.dharamkanta.utils.PermissionHelper;
import com.canvalabs.dharamkanta.utils.Validate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    PermissionHelper permissionHelper;
    static private String TAG = "LoginActivity";
    String username, password;
    SharedPreferences prefs;

    private boolean isPermissionGranted = false;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location location;
    boolean doubleBackToExitPressedOnce = false;
    Button btn_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((TextView) findViewById(R.id.tncLink)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) findViewById(R.id.tncLink)).setText(Html.fromHtml("By signing up, You agree to the <a href=\"https://www.google.com/\">Terms & Conditions</a> and <a href=\"https://www.google.com/\">Privacy Policy</a> of Canvalabs"));
        ((TextView) findViewById(R.id.demoLink)).setText(Html.fromHtml("Want to try us? Get a free <a href=\"https://www.google.com/\">Demo</a> scheduled"));
        CustomTextView forgotPass = (CustomTextView) findViewById(R.id.forgot_password);
        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(LoginActivity.this, "This feature is not supported yet!", Toast.LENGTH_SHORT).show();
            }
        });
        prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        String[] needed_permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        permissionHelper=new PermissionHelper(this).setListener(new PermissionHelper.PermissionsListener() {
            @Override
            public void onPermissionGranted(int request_code) {

                isPermissionGranted = true;

            }

            @Override
            public void onPermissionRejectedManyTimes(@NonNull List<String> rejectedPerms, int request_code) {
                isPermissionGranted = false;

                Common.showDialogue(LoginActivity.this, "To use this app you need to allow the requested permissions");
            }

        });

        permissionHelper.requestPermission(needed_permissions, 100);

        btn_signin = findViewById(R.id.btn_signin);
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        ImageButton exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void goToLogin() {
//        mProgress.show();


        EditText editTextUsername = findViewById(R.id.username);
        username = editTextUsername.getText().toString();
        if(!Validate.isValidMobile(username)){
            Toast.makeText(this, "Invalid username", Toast.LENGTH_SHORT).show();
            return;
        }

        EditText editTextPassword = findViewById(R.id.password);
        password = editTextPassword.getText().toString();
        if(!Validate.isValidPassword(password)){
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show();
            return;
        }
        btn_signin.setText("SIGNING IN...");
        btn_signin.setEnabled(false);
        String url = "http://101.53.157.211/api/portal/login";

        Map<String, String> params = new HashMap<>();
        params.put("mobile", username);
        params.put("password", password);
        params.put("latitude", "11.1111");
        params.put("longitude", "11.1111");

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,

                url, parameters,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if(!obj.getString("statusCode").equals("200")){
//                                mProgress.dismiss();
                                btn_signin.setEnabled(true);
                                btn_signin.setText("SIGN IN");
                                Common.showDialogue(LoginActivity.this, obj.getString("statusDesc"));
                            }else{
//                                mProgress.dismiss();
                                btn_signin.setEnabled(true);
                                btn_signin.setText("SIGN IN");
                                Log.d(TAG, obj.getString("data"));
                                JSONObject data = new JSONObject(obj.getString("data"));
                                SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE).edit();
                                editor.putString("user_id", data.getString("id"));
                                editor.putString("name", data.getString("name"));
                                editor.putString("company", data.getString("company"));
                                editor.putString("company_type", data.getString("company_type"));
                                editor.putString("email", data.getString("email"));
                                editor.putString("app_token", username+":"+data.getString("app_token"));
                                editor.apply();

                                Intent i = new Intent(LoginActivity.this, WelcomeActivity.class);
                                startActivity(i);

                                // close this activity
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error activity",error.toString());

//                mProgress.dismiss();

                if(error.networkResponse == null){
                    Common.showNetworkError(LoginActivity.this, 0);
                }else if(error.networkResponse.statusCode != 200){
                    Common.showNetworkError(LoginActivity.this, error.networkResponse.statusCode);
                }
                btn_signin.setEnabled(true);
                btn_signin.setText("SIGN IN");

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = username+":"+password;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE).edit();
        editor.putString("latitude", "22,9876");
        editor.putString("longitude", "77,9876");
        editor.apply();

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location loc) {
        location = loc;
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }


}

