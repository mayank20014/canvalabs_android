package com.canvalabs.dharamkanta.Model;

import org.json.JSONObject;

public class CollectionObject extends JSONObject {

    public String id;
    public String type_name;
    public String icon_url;
    public String  is_active;
    public int count;
    public double amount;

    public CollectionObject(){
        super();
    }

    public String getId() {
        return id;
    }

    public String getType_name() {
        return type_name;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public String getIs_active() {
        return is_active;
    }

    public int getCount() {
        return count;
    }

    public double getAmount() {
        return amount;
    }

}